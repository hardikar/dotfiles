# Firefox plugins
* Adblock plus
* Classic theme restorer
* FireSSH
* Vimperator
* Tile tabs

# Applications
* iTerm2
* Firefox
* Seil
* Karabiner
* Slate
* HomeBrew
